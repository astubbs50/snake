﻿var canvas;
var context;


var snakeSectionWidth = 5;
var snakeWidth = 8;
var snakes = [];
var da = 0;
var target = {};
var snakeMaxTurn = 0.1;
var addSnakeLength = 0;
var gameTime = 35;
var gameBaseTime = 50;
var level;
var levelNum = 0;
var circleHitBoxes;
var rectangleHitBoxes;
var lives = 5;
var startingLives = 5;

var eggs = [];
var magic_eggs = [];
var score = 0;
var eggPoints = 10;
var scoreTexts = [];
var showScoreDuration = 30;
var magic_eggs_duration = 200;

var camera = {
    tileWidth: 127,
    tileHeight: 127,
    tileHalfWidth: 64,
    tileHalfHeight: 64,
    tileX: 0,
    tileY: 0,
    offsetX: 64,
    offsetY: 64,
    scale: 2
};

var tile_images = [];
var tilesSrc = [
	"/images/land2.png",
	"/images/land1.png",
	"/images/land3.png",
	"/images/brickroad.png",
	"/images/muddy1.png",
	"/images/muddy2.png",
	"/images/andy_new_river1.png",
	"/images/andy_new_river2.png",
	"/images/andy_new_river3.png",
	"/images/andy_new_river4.png",
	"/images/fence1.png",
	"/images/fence2.png",
	"/images/fence3.png",
	"/images/fence4.png",
	"/images/fence5.png",
	"/images/fence6.png",
	"/images/fence7.png",
	"/images/fence8.png",
	"/images/fence9.png",
	"/images/fence10.png"
];

var item_images = [];
var itemsSrc = [
	"/images/blank.png",
	"/images/tractor.png",
	"/images/tractor2.png",
	"/images/pond.png",
	"/images/mud.png",
	"/images/mud2.png",
	"/images/mud3.png",
	"/images/apple.png",
	"/images/shovel.png",
	"/images/haybale.png"
];

var snake_images = [];
var imgSnakeSrc = [
    "/images/snake_01.png",
    "/images/snake_02.png",
    "/images/snake_03.png",
    "/images/snake_04.png",
    "/images/snake_head.png",
    "/images/icon.png",
    "/images/egg.png",
    "/images/magic_egg.png"
];

function Init()
{
    canvas = document.getElementById("myCanvas");
    canvas.width = canvas.clientWidth;
    canvas.height = canvas.clientHeight;
    canvas.addEventListener("mousedown", OnCanvasMouseDown, false);
    context = canvas.getContext("2d");

    camera.tileCountWidth = canvas.width / camera.tileWidth;
    camera.tileCountHeight = canvas.height / camera.tileHeight;
    camera.screenScrollLeft = canvas.width - canvas.width / 5;
    camera.screenScrollRight = canvas.width / 5;
    camera.screenScrollDown = canvas.height - canvas.height / 5;
    camera.screenScrollUp = canvas.height / 5;

    InitImages(imgSnakeSrc, snake_images);
    InitImages(tilesSrc, tile_images);
    InitImages(itemsSrc, item_images);
}

var loads = 3;
var loadCount = 0;
function InitImages(imgSources, images)
{    
    var imgCount = 0;    
    for (var src in imgSources)
    {
        var img = new Image();
        img.onload = function ()
        {
            imgCount++;           
            if (imgCount === imgSources.length)
            {
                loadCount++;
                if (loadCount == loads)
                {
                    level = levels[levelNum];
                    IntroLevel();
                }                
            }
        }
        
        img.src = imgSources[src];
        images.push(img);
    }

}

function InitSnake(length, x, y, baseScale)
{
    var snakeBody = [];
    var a = 0;
    var da = 0.17;
    var dda = 0;

    if (!baseScale)
        baseScale = (length / 200) + 0.5;
    var dScale = 0;
    var scale = baseScale;
    //scale = 1;
    var tailStart = 30;

    for (var i = 0; i < length; i++)
    {
        snakeBody.push({
            x: x,
            y: y,
            a: a,
            scale: scale
        });
        x -= Math.cos(a) * snakeSectionWidth * scale;
        y -= Math.sin(a) * snakeSectionWidth * scale;
        a += da;
        da += dda;
        
        if (i >= length * .66 || i > length - tailStart)
        {
            //dScale = (baseScale) / (length * .33) * -1;
            //dScale = (baseScale) * (length - i) / length;
            scale = (baseScale) * (length - i) / (tailStart);
        }
        else
            scale = baseScale;
    }

    return snakeBody;
}

function IntroLevel()
{
    InitLevel();

    SetStartLocation();
    DrawMap();
    DrawItems();

    context.fillStyle = "rgba(33, 66, 99, 0.5)";
    context.fillRect(0, 0, canvas.width, canvas.height);
    context.fillStyle = "white";
    context.font = "60 pt Calibri";
    context.textAlign = "center";
        
    var strMessage = "Level " + (levelNum + 1);

    context.fillText(strMessage, canvas.width / 2, canvas.height / 2 - 35);    
    StartCountDown(canvas.height / 2 + 55, 3);    
}
var bStartLocationSet = false;

function SetStartLocation()
{
    target.x = (canvas.width / 2);
    target.y = (canvas.height / 2);

    var startLocations = [];
    for (var i = level.items.length - 1; i >= 0; i--)
    {
        if (level.items[i].index == 0)
        {
            startLocations.push(level.items[i]);            
        }
    }

    if (startLocations.length > 0)
    {
        var index = Math.floor(Math.random() * startLocations.length);
        var startLocation = startLocations[index];
        var x = startLocation.x - (canvas.width / 2) / camera.scale;
        var y = startLocation.y - (canvas.height / 2) / camera.scale;
        camera.tileX = Math.floor(x / camera.tileWidth);
        camera.tileY = Math.floor(y / camera.tileHeight);
        camera.offsetX = (x / camera.tileWidth - camera.tileX) * camera.tileWidth;
        camera.offsetY = (y / camera.tileHeight - camera.tileY) * camera.tileHeight;

        if (camera.tileX < 0)
        {
            camera.tileX = 0;
            camera.offsetX = 127;
        }
        if(camera.tileY < 0)
        {
            camera.tileY = 0;
            camera.offsetY = 127;
        }        
        
    }

    bStartLocationSet = true;
}

function Start()
{
    if (!bStartLocationSet)
        SetStartLocation();

    bStartLocationSet = false;
    snakes = []; 
    
    var snake = {
        body: InitSnake(50, target.x, target.y, 1),
        da: 0,
        lastTurn: 0
    }
    snakes.push(snake);

    Run();
}

function InitLevel()
{
    circleHitBoxes = [];
    rectangleHitBoxes = [];
    for(var i = 0; i < level.hitBoxes.length; i++)
    {
        if(level.hitBoxes[i].type == "Circle")
        {
            circleHitBoxes.push(level.hitBoxes[i]);
        }
        else if(level.hitBoxes[i].type == "Rectangle")
        {
            rectangleHitBoxes.push(level.hitBoxes[i]);
        }
    }

    var eggCount = level.eggCount;

    for (var i = 0; i < eggCount; i++)
    {
        var egg = CreateEgg();

        eggs.push(egg);
    }
}

function CreateEgg()
{
    var bPlaced = false;
    var x, y;
    while (!bPlaced)
    {
        x = Math.floor(Math.random() * level.data.length * camera.tileWidth);
        y = Math.floor(Math.random() * level.data.length * camera.tileHeight);

        bPlaced = true;
        for (var j = 0; j < level.hitBoxes.length; j++)
        {
            var x2, y2, radius;
            if (level.hitBoxes[j].type == "Circle")
            {
                x2 = level.hitBoxes[j].x;
                y2 = level.hitBoxes[j].y;
                radius = level.hitBoxes[j].radius * 3;
                var dx = x2 - x;
                var dy = y2 - y;
                var d = Math.sqrt(dx * dx + dy * dy);
                if (d < radius + 5)
                {
                    bPlaced = false;
                }
            }
            else if (level.hitBoxes[j].type == "Rectangle")
            {
                var width = (level.hitBoxes[j].x2 - level.hitBoxes[j].x1);
                var height = (level.hitBoxes[j].y2 - level.hitBoxes[j].y1);
                
                left1 = x - 15;
                right1 = x + 15;
                top1 = y - 15;
                bottom1 = y + 15;
               
                var left2 = level.hitBoxes[j].x1;
                var top2 = level.hitBoxes[j].y1;
                var right2 = level.hitBoxes[j].x2;
                var bottom2 = level.hitBoxes[j].y2;
                
                if (top2 < bottom1 && bottom2 > top1 && left2 < right1 && right2 > left1)      
                {
                    bPlaced = false;
                }
            }
        }

        for(var i = 0; i < level.items.length; i++)
        {
            x2 = level.items[i].x;
            y2 = level.items[i].y;
            var img = item_images[level.items[i].index];
            radius = Math.sqrt(img.width * img.width + img.height * img.height) / 2.5;
            var dx = x2 - x;
            var dy = y2 - y;
            var d = Math.sqrt(dx * dx + dy * dy);
            if (d < radius + 5)
            {
                bPlaced = false;
            }
        }
    }

    return {
        x: x,
        y: y,
        radius: 5,
        points: eggPoints
    };
}

function Draw()
{
    context.clearRect(0, 0, canvas.width, canvas.height);
    DrawMap();
    DrawEggs();
    DrawSnake(snakes[0].body);
    DrawItems();
    DrawScoreTexts();
    DrawIcons();

    context.save();
    context.strokeStyle = "rgba(203, 12, 23, 0.25)";
    context.lineCap = "round";
    context.lineWidth = 5;
    context.translate(target.x, target.y);
    context.beginPath();
    context.moveTo(-10, -10);
    context.lineTo(10, 10);
    context.moveTo(-10, 10);
    context.lineTo(10, -10);
    context.stroke();
    context.restore();
}

function Run()
{    
    Draw();
    
    MoveSnake(snakes[0]);

    context.fillStyle = "rgba(33, 66, 99, 0.5)";
    if (DetectCollisions(snakes[0].body))
    {        
        lives--;
        Draw();
        context.fillStyle = "rgba(33, 66, 99, 0.5)";
        context.fillRect(0, 0, canvas.width, canvas.height);
        context.fillStyle = "white";
        context.font = "60 pt Calibri";
        context.textAlign = "center";
        if (lives > 0)
        {
            var strMessage = "Collision!";
            var strMessage2;

            if(lives == 1)
                strMessage2 = lives + " Snake Left";
            else
                strMessage2 = lives + " Snakes Left";
            
            context.fillText(strMessage, canvas.width / 2, canvas.height / 2 - 35);
            context.fillText(strMessage2, canvas.width / 2, canvas.height / 2 + 35);
            StartCountDown(canvas.height / 2 + 55, 3);
        }
        else
        {
            var strMessage = "Game Over!";
            context.fillText(strMessage, canvas.width / 2, canvas.height / 2 - 35);
            
            levelNum = 0;
            score = 0;
            level = levels[levelNum];
            lives = startingLives;

            setTimeout(IntroLevel, 3000);
        }       
    }        
    else if (DetectCollisionWithEggs(snakes[0].body))
    {
        Draw();
        context.fillStyle = "rgba(33, 66, 99, 0.5)";
        context.fillRect(0, 0, canvas.width, canvas.height);
        context.fillStyle = "white";
        context.font = "60 pt Calibri";
        context.textAlign = "center";

        context.fillText("Level Completed!", canvas.width / 2, canvas.height / 2 - 35);

        //Level Cleared
        levelNum++;
        level = levels[levelNum];

        setTimeout(IntroLevel, 3000);
    }
    else
        setTimeout(Run, gameTime);
}

var countDownTicks = 0;
var countDownDiv;
function StartCountDown(height, count)
{
    countDownTicks = count;
    countDownDiv = document.createElement("div");
    document.body.appendChild(countDownDiv);
    countDownDiv.style.color = "white";
    countDownDiv.style.fontFamily = "calibri";
    countDownDiv.style.fontSize = "70pt";
    countDownDiv.style.fontWeight = "bold";
    countDownDiv.style.position = "absolute";
    countDownDiv.style.textAlign = "center";
    countDownDiv.style.top = height + "px";
    countDownDiv.style.width = 100 + "px";
    countDownDiv.style.left = canvas.width / 2 - 50 + "px";
    
    countDownDiv.innerHTML = count;

    CountDown();
}

function CountDown()
{
    countDownDiv.innerHTML = countDownTicks;
    countDownTicks--;

    if(countDownTicks >= 0)
        setTimeout(CountDown, 1000);
    else
    {
        document.body.removeChild(countDownDiv);
        //setTimeout(Start, 1000);
        Start();
    }    
}

function DrawIcons()
{
    context.fillStyle = "rgba(33, 66, 99, 0.5)";
    context.fillRect(0, 0, canvas.width, 80);
    for(var i = 0; i < lives; i++)
    {        
        context.drawImage(snake_images[5], canvas.width - 50 - i * 50, 15);
    }

    
    context.fillStyle = "white";
    context.font = "30 pt Calibri";
    context.textAlign = "left";
    var metrics = context.measureText(eggs.length);
    context.drawImage(snake_images[6], 20 + metrics.width, 30);
    context.fillText(eggs.length, 15, 50);
    context.fillText("Remaining", 45 + metrics.width, 50);
    context.fillText(score, canvas.width / 2, 50);
}

function DrawMap()
{
    for (var x = camera.tileX; x < camera.tileX + camera.tileCountWidth && x < level.data.length; x++)
    {
        for(var y = camera.tileY; y < camera.tileY + camera.tileCountHeight && y < level.data[0].length; y++)
        {
            var tile = level.data[x][y];
            context.save();
            context.scale(camera.scale, camera.scale);
            context.translate((x - camera.tileX) * camera.tileWidth + camera.tileHalfWidth, (y - camera.tileY) * camera.tileHeight + camera.tileHalfHeight);
            context.translate(-camera.offsetX, -camera.offsetY);
            context.rotate(tile.rotation);
            context.drawImage(tile_images[tile.index], -camera.tileHalfWidth, -camera.tileHalfHeight);
            context.restore();
        }
    }
}

function DrawItems()
{
    for(var i = 0; i < level.items.length; i++)
    {
        var image = item_images[level.items[i].index];
        context.save();
        context.scale(camera.scale, camera.scale);
        context.translate(level.items[i].x - camera.tileX * camera.tileWidth - camera.offsetX, level.items[i].y - camera.tileY * camera.tileHeight - camera.offsetY);
        context.save();
        context.rotate(level.items[i].rotation);
        context.drawImage(image, -image.width / 2, -image.height / 2);
        context.restore();
        context.restore();
    }
}

function DrawEggs()
{
    var cx = canvas.width / 2;
    var cy = canvas.height / 2;
    var cr;
    if(canvas.width > canvas.height)
        cr = canvas.height / 2 - 50;
    else
        cr = canvas.width / 2 - 50;

    for (var i = 0; i < eggs.length; i++)
    {
        var x = eggs[i].x + 7;
        var y = eggs[i].y + 7;

        var x2 = (eggs[i].x - camera.tileX * camera.tileWidth - camera.offsetX) * camera.scale;
        var y2 = (eggs[i].y - camera.tileY * camera.tileHeight - camera.offsetY) * camera.scale;

        if ( eggs.length <= 3 && (x2 > canvas.width || y2 > canvas.height || x2 < 0 || y2 < 0))
        {
            var dx = x2 - cx;
            var dy = y2 - cy;
            var a = Math.atan2(dy, dx);
            var arrowX = cx + Math.cos(a) * cr;
            var arrowY = cy + Math.sin(a) * cr;

            context.save();            
            context.strokeStyle = "rgba(128, 88, 88, 0.3)";
            context.lineWidth = 10;
            context.lineCap = "round";
            context.translate(arrowX, arrowY);            
            context.rotate(a);
            context.beginPath();
            context.moveTo(0, 0);
            context.lineTo(-30, 0);
            context.moveTo(0, 0);
            context.lineTo(-20, -20);
            context.moveTo(0, 0);
            context.lineTo(-20, 20);
            context.stroke();
            context.restore();
       }
       else
       {
            context.save();
            context.scale(camera.scale, camera.scale);
            context.translate(x - camera.tileX * camera.tileWidth - camera.offsetX, y - camera.tileY * camera.tileHeight - camera.offsetY);
            context.scale(1 / camera.scale, 1 / camera.scale);
            context.drawImage(snake_images[6], -25, -25);
            context.restore();
       }        
    }

    for (var i = magic_eggs.length - 1; i >= 0; i--)
    {
        var x = magic_eggs[i].x + 7;
        var y = magic_eggs[i].y + 7;        

        context.save();
        context.scale(camera.scale, camera.scale);
        context.translate(x - camera.tileX * camera.tileWidth - camera.offsetX, y - camera.tileY * camera.tileHeight - camera.offsetY);
        context.scale(1 / camera.scale, 1 / camera.scale);
        context.globalAlpha = magic_eggs[i].duration / magic_eggs_duration;
        context.drawImage(snake_images[7], -25, -25);
        context.restore();

        magic_eggs[i].duration--;
        if(magic_eggs[i].duration === 0)
        {
            magic_eggs.splice(i, 1);
        }
    }

    if (Math.random() < level.magicEggRate)
    {
        var magic_egg = CreateEgg();
        magic_egg.points = Math.floor(Math.random() * 50) * 2 + 20;
        magic_egg.duration = magic_eggs_duration;
        magic_eggs.push(magic_egg);
    }
}

function DrawScoreTexts()
{
    
    for(var i = scoreTexts.length - 1; i >= 0; i--)
    {
        context.save();
        context.scale(camera.scale, camera.scale);
        context.translate(scoreTexts[i].x - camera.tileX * camera.tileWidth - camera.offsetX, scoreTexts[i].y - camera.tileY * camera.tileHeight - camera.offsetY);
        context.font = "18 pt Calibri";
        context.fillStyle = "rgba(255, 255, 255, " + scoreTexts[i].duration / showScoreDuration + ")";
        context.fillText(scoreTexts[i].str, 0, 0);
        context.restore();
        scoreTexts[i].duration--;
        scoreTexts[i].y--;        

        if (scoreTexts[i].duration <= 0)
            scoreTexts.splice(i, 1);
    }
}

function DetectCollisions(snakeBody)
{
    if (DetectCollisionWithSelf(snakeBody))
        return true;
    if (DetectCollisionWithItems(snakeBody))
        return true;

    return false;
}

function DetectCollisionWithSelf(snakeBody)
{
    var snakeHead = snakeBody[0];
    var radius1 = snakeHead.scale * snakeWidth * 1.25;
    //context.beginPath();
    //context.arc(snakeHead.x + 4, snakeHead.y + 8, radius1, 0, Math.PI * 2, false);
    //context.fill();
    for(var i = 50; i < snakeBody.length; i+=5)
    {
        var snakePart = snakeBody[i];

        var radius2 = snakePart.scale * snakeWidth;
        //context.beginPath();
        //context.arc(snakePart.x + 4, snakePart.y + 8, radius2, 0, Math.PI * 2, false);
        //context.fill();

        var dx = snakePart.x - snakeHead.x;
        var dy = snakePart.y - snakeHead.y;
        var d2 = dx * dx + dy * dy;

        if (d2 < (radius1 + radius2) * (radius1 + radius2))
            return true;
    }

    return false;
}

function DetectCollisionWithItems(snakeBody)
{
    var snakeHead = snakeBody[0];
    var radius1 = snakeHead.scale * snakeWidth * 1.25 / camera.scale;        
    var x1 = (snakeHead.x + 4) / camera.scale + camera.tileX * camera.tileWidth + camera.offsetX;
    var y1 = (snakeHead.y + 10) / camera.scale + camera.tileY * camera.tileHeight + camera.offsetY;
    
    var dSize = radius1 * 2 * camera.scale;
    var size = radius1 * 2;
    var left1 = x1 - size / 2;
    var top1 = y1 - size / 2;
    var right1 = left1 + size;
    var bottom1 = top1 + size;

    for(var i = 0; i < circleHitBoxes.length; i++)
    {
        var dx = circleHitBoxes[i].x - x1;
        var dy = circleHitBoxes[i].y - y1;

        var x2 = circleHitBoxes[i].x - camera.tileX * camera.tileWidth - camera.offsetX;
        var y2 = circleHitBoxes[i].y - camera.tileY * camera.tileHeight - camera.offsetY;

        /*context.save();
        context.beginPath();
        context.scale(camera.scale, camera.scale);
        context.fillStyle = "rgba(255,255,128, 0.5)";
        context.arc(x2, y2, circleHitBoxes[i].radius, 0, Math.PI * 2, false);
        context.fill();
        context.restore();
        */
        var d2 = dx * dx + dy * dy;
        if (d2 < (radius1 + circleHitBoxes[i].radius) * (radius1 + circleHitBoxes[i].radius))
        {
            /*context.save();
            context.beginPath();
            context.scale(camera.scale, camera.scale);
            context.fillStyle = "rgba(255,0,0, 0.5)";
            context.arc(x2, y2, circleHitBoxes[i].radius, 0, Math.PI * 2, false);
            context.fill();
            context.restore();
            */
            return true;
        }                  
    }

    for (var i = 0; i < rectangleHitBoxes.length; i++)
    {
        var left2d = rectangleHitBoxes[i].x1 - camera.tileX * camera.tileWidth - camera.offsetX;
        var top2d = rectangleHitBoxes[i].y1 - camera.tileY * camera.tileHeight - camera.offsetY;
        var right2d = rectangleHitBoxes[i].x2 - camera.tileX * camera.tileWidth - camera.offsetX;
        var bottom2d = rectangleHitBoxes[i].y2 - camera.tileY * camera.tileHeight - camera.offsetY;
        var left2 = rectangleHitBoxes[i].x1;
        var top2 = rectangleHitBoxes[i].y1;
        var right2 = rectangleHitBoxes[i].x2;
        var bottom2 = rectangleHitBoxes[i].y2;

        /*context.save();        
        context.scale(camera.scale, camera.scale);
        context.fillStyle = "rgba(255, 0, 255, 0.5)";
        context.fillRect(left2d, top2d, right2d - left2d, bottom2d - top2d);
        context.restore();
        */
        if (top2 < bottom1 && bottom2 > top1 && left2 < right1 && right2 > left1)            
        {
          /*  context.save();
            context.scale(camera.scale, camera.scale);
            context.fillStyle = "rgba(0, 0, 0, 0.9)";
            context.fillRect(left2d, top2d, right2d - left2d, bottom2d - top2d);
            context.restore();
            */
            return true;
        }            
    }

    return false;
}

function DetectCollisionWithEggs(snakeBody)
{
    var snakeHead = snakeBody[0];
    var radius1 = snakeHead.scale * snakeWidth * 1.25 / camera.scale;
    var x1 = (snakeHead.x + 4) / camera.scale + camera.tileX * camera.tileWidth + camera.offsetX;
    var y1 = (snakeHead.y + 10) / camera.scale + camera.tileY * camera.tileHeight + camera.offsetY;

    CheckEggs(eggs, x1, y1, radius1);
    CheckEggs(magic_eggs, x1, y1, radius1);


    if (eggs.length == 0)
    {
        return true;
    }

    return false;
}

function CheckEggs(eggs, x1, y1, radius1)
{
    for (var i = 0; i < eggs.length; i++)
    {
        var dx = eggs[i].x - x1;
        var dy = eggs[i].y - y1;

        var x2 = eggs[i].x - camera.tileX * camera.tileWidth - camera.offsetX;
        var y2 = eggs[i].y - camera.tileY * camera.tileHeight - camera.offsetY;

        var d2 = dx * dx + dy * dy;
        if (d2 < (radius1 + eggs[i].radius) * (radius1 + eggs[i].radius))
        {
            if (eggs[i].points > 100)
            {
                scoreTexts.push({
                    str: "+ 1 Snake",
                    x: eggs[i].x,
                    y: eggs[i].y,
                    duration: showScoreDuration
                });
                lives++;
            }
            else
            {
                scoreTexts.push({
                    str: "+" + eggs[i].points,
                    x: eggs[i].x,
                    y: eggs[i].y,
                    duration: showScoreDuration
                });
                score += eggs[i].points;

                GrowSnake(eggs[i].points);
            }
            
            eggs.splice(i, 1);
        }
    }
}

function TurnSnake(snake)
{
    var snakeBody = snake.body;
    var dx = snakeBody[0].x - target.x;
    var dy = snakeBody[0].y - target.y;
    var a = Math.atan2(dy, dx);
    var d = dx * dx + dy * dy;
    
    if (d < 10000)
        snake.lastTurn = 1;
    else
        snake.lastTurn = 8;

    if (a < snakeBody[0].a)
    {
        if (snakeBody[0].a - a < (Math.PI * 2) - snakeBody[0].a + a)
            da = snakeMaxTurn;
        else
            da = -snakeMaxTurn;
    }
    else
    {
        if (a - snakeBody[0].a < (Math.PI * 2) - a + snakeBody[0].a)
            da = -snakeMaxTurn;
        else
            da = snakeMaxTurn;
    }

    return da;
}

function MoveSnakeHead(snakeBody, da)
{
    var snakePart = snakeBody[0];
    snakePart.a += da;
    if (snakePart.a > Math.PI)
        snakePart.a -= Math.PI * 2;
    if (snakePart.a < -Math.PI)
        snakePart.a += Math.PI * 2;

    var oldX = snakePart.x;
    var oldY = snakePart.y;

    snakePart.x += Math.cos(snakePart.a) * snakeSectionWidth;// * snakePart.scale;
    snakePart.y += Math.sin(snakePart.a) * snakeSectionWidth;// * snakePart.scale;
    var x = snakePart.x;
    var y = snakePart.y;

    //Scroll Screen left
    if(x > camera.screenScrollLeft)
    {        
        camera.offsetX += (x - oldX) / camera.scale;
        target.x -= (x - oldX);

        if (camera.offsetX > camera.tileWidth && camera.tileX < level.data.length)
        {
            camera.offsetX = 0;
            camera.tileX++;
        }

        snakePart.x = oldX;
    }
    //Scroll Screen Right
    else if(x < camera.screenScrollRight)
    {
        camera.offsetX += (x - oldX) / camera.scale;
        target.x -= (x - oldX);

        if(camera.offsetX < 0 && camera.tileX > 0)
        {     
            camera.tileX--;
            camera.offsetX = camera.tileWidth;
        }

        snakePart.x = oldX;
    }

    //Scroll Screen Down
    if(y > camera.screenScrollDown)
    {
        camera.offsetY += (y - oldY) / camera.scale;
        target.y -= (y - oldY);
        if (camera.offsetY > camera.tileHeight && camera.tileY < level.data[0].length)
        {
            camera.tileY++;
            camera.offsetY = 0;
        }

        snakePart.y = oldY;
    }
    //Scroll Screen Up
    else if (y < camera.screenScrollUp)
    {
        camera.offsetY += (y - oldY) / camera.scale;
        target.y -= (y - oldY);
        if (camera.offsetY < 0 && camera.tileY > 0)
        {
            camera.tileY--;
            camera.offsetY = camera.tileHeight;
        }

        snakePart.y = oldY;
    }
}

function MoveSnakeBody(snakeBody, da)
{
    if (addSnakeLength > 0)
    {
        snakeBody.splice(0, 0,
        {
            x: snakeBody[0].x,
            y: snakeBody[0].y,
            a: snakeBody[0].a,
            scale: snakeBody[0].scale
        });       
        addSnakeLength--;        
    }
    else
    {
        shiftFrom = 0;
        for (var i = snakeBody.length - 1; i > 0; i--)
        {
            snakeBody[i].a = snakeBody[i - 1].a;
        }
    }

    var snakePart = snakeBody[0];

    MoveSnakeHead(snakeBody, da);
    var x = snakePart.x;
    var y = snakePart.y;

    for (var i = 1; i < snakeBody.length; i++)
    {
        var snakePart = snakeBody[i];
        x -= Math.cos(snakePart.a) * snakeSectionWidth;// * snakePart.scale;
        y -= Math.sin(snakePart.a) * snakeSectionWidth;// * snakePart.scale;
        snakePart.x = x;
        snakePart.y = y;
    }        
}

function MoveSnake(snake)
{
    if (snake.lastTurn <= 0)
    {        
        //da = Math.random() * 0.2 - 0.1;
        snake.da = TurnSnake(snake);
    }
    MoveSnakeBody(snake.body, snake.da);

    snake.lastTurn--;
}

function DrawSnake(snakeBody)
{
    var snakePart = snakeBody[0];
    context.save();
    context.translate(snakePart.x + 4, snakePart.y + 10);   
    context.rotate(snakePart.a);
    context.scale(1, snakePart.scale);
    
    context.drawImage(snake_images[4], -4, -10);
    context.restore();

    for (var i = 1; i < snakeBody.length; i++)
    {
        var snakePart = snakeBody[i];
        context.save();        
        context.translate(snakePart.x + 4, snakePart.y + 8);        
        context.rotate(snakePart.a);
        context.scale(1, snakePart.scale);
        
        context.drawImage(snake_images[3 - (i % 4)], -4, -8);
        context.restore();
    }
}

function OnCanvasMouseDown(e)
{
    target.x = e.pageX;
    //target.y = e.pageY - snakes[0][0].scale * 8;
    target.y = e.pageY;
}

function GrowSnake(length)
{
    addSnakeLength += length;    
}

function SlowMow()
{
    if (gameTime == 350)
        gameTime = 35
    else
        gameTime = 350;
}